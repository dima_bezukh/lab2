﻿using System;
using System.Collections.Generic;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            ShingleResolver _resolver = new ShingleResolver();
            List<(List<(string, double)>, int)> textToHtml = new List<(List<(string, double)>, int)>();

            int[] shinglesSizes = new int[] {1, 2, 3, 5 };
            foreach(var size in shinglesSizes)
            {
                textToHtml.Add((_resolver.CheckFile("FileToCheck.txt", size), size));
            }

            _resolver.GenerateHtml(textToHtml);
        }
    }
}
