﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Lab2
{
    public class ShingleResolver
    {
        private const string dbDirectoryPath = @"D:\Магістратура\1M\Tkachenko\Lab2\Lab2\db";
        private const string stopwordsfileName = "stopwotds.txt";

        protected string[] stopWords;
        private List<string> filesName = new List<string>() { "1.txt", "2.txt", "3.txt", "4.txt", "5.txt" };

        static Semaphore sem = new Semaphore(1, 1);
        public ShingleResolver()
        {

           stopWords = ReadStopWords();
        }

        private string ReadstringFromFile(string filepath)
        {
            return File.ReadAllText(filepath, Encoding.UTF8);
        }
        private string[] ReadStopWords()
        {
            string words = ReadstringFromFile(Path.Combine(dbDirectoryPath, stopwordsfileName));
            return words.Split(new char[] { '\n' }).Select(p => p.Trim(new char[] { '\r' })).ToArray();
        }

        public IList<string> Canonize(string text)
        {
            char[] splitSympols = new char[] { ' ', '\t', '\r', '\n', '.', ',', '!', '?', ':', ';', '-', '(', ')' };
            var words = text.Split(splitSympols).Select(w => w.ToLower().Trim(' ', '\t')).Where(w => !String.IsNullOrWhiteSpace(w));

            return words.Where(w => !stopWords.Contains(w)).ToList();
        }

        //sd aasd asd sad ads ads asd asd 
        protected IEnumerable<string> GetShingles(IList<string> words, int shingleSize)
        {
            var length = words.Count - shingleSize + 1;

            if (length <= 0)
                throw new Exception("Not enough length");

            var shingles = new string[length];

            for (int i = 0; i < shingles.Length; i++)
                shingles[i] = String.Join(" ", getSubArray(words, i, shingleSize));

            return shingles;
        }
        protected IEnumerable<string> GetHashes(IList<string> shingles)
        {
            return shingles.Select(p => p.GetHashCode().ToString());
        }

        protected async Task<IEnumerable<string>> GetHashesAsync(IList<string> shingles)
        {
            return await Task.Run(() => GetHashes(shingles));
        }

        protected double Compare(IEnumerable<string> firstShingles, IEnumerable<string> secondShingles)
        {
            double sameShingles = 0;
            foreach (var shingle in firstShingles)
                if (secondShingles.Contains(shingle))
                    sameShingles++;
            return sameShingles / firstShingles.ToList().Count();
        }

        public async Task<double> CalculatePlagiarism(string firstText, string secondText, int shingleSize)
        {
            IEnumerable<string> firstTextShinhles = GetShingles(Canonize(firstText), shingleSize);
            IEnumerable<string> secondTextShinhles = GetShingles(Canonize(secondText), shingleSize);

            IEnumerable<string> firstShinglesHashes = await GetHashesAsync(firstTextShinhles.ToList());
            IEnumerable<string> secondShinglesHashes = await GetHashesAsync(secondTextShinhles.ToList());

            return Compare(firstShinglesHashes, secondShinglesHashes);
        }

        public List<(string, double)> CheckFile(string fileName, int shingleSize)
        {
            List<(string, double)> result = new List<(string, double)>();

            string input = ReadstringFromFile(Path.Combine(dbDirectoryPath, fileName));

            Task[] tasks = new Task[filesName.Count];
            for(int i=0; i<5; i++)
            {
                int j = i;
                tasks[j] = Task.Run(async () =>
                {
                    double percent = await CalculatePlagiarism(input,
                                                         ReadstringFromFile(Path.Combine(dbDirectoryPath, filesName[j])), 
                                                         shingleSize);

                    sem.WaitOne();
                    result.Add((filesName[j], percent));
                    sem.Release();
                });
            }

            Task.WaitAll(tasks);

            return result.OrderBy(p=>p.Item1).ToList();
        }

        public void GenerateHtml(List<(List<(string, double)>, int)> data)
        {
            StringBuilder html = new StringBuilder();
            html.Append("<style>.box{display: flex;justify-content: center;} th, td{border: 1px solid red; padding: 2px;} table {margin: 0 5px;}</style>");
            html.Append("<h1 style=\"text-align: center; \">Plagiarism report</h1>");
            html.Append("<div class=\"box\">");
            foreach(var item in data)
            {
                html.Append("<table>");
                html.Append($"<tr><th colspan=\"2\">Shingles size - {item.Item2}</th>");
                html.Append("<tr><th>File name</th><th>Percent of plagiarism</th></tr>");
                html.Append(string.Join("", item.Item1.Select(p => "<tr><td>" + p.Item1 + "</td><td>" + p.Item2.ToString() + "</td></tr>")));
                html.Append("</table>");
            }
            html.Append("</div>");

            File.WriteAllText(@"D:\test.html", html.ToString());
        }




        private IEnumerable<string> getSubArray(IList<string> words, int startIndex, int size)
        {
            var selected = new string[size];

            for (int i = 0; i < size; i++)
                selected[i] = words[i + startIndex];

            return selected;
        }
    }
}
